#include "array.h"
#include <stdio.h>
#include <stdlib.h>

struct ArrayData* initArray() {
	struct ArrayData* newArray = (ArrayData *) malloc(sizeof(struct ArrayData)); ///results in to C2440 hence converting to type
	newArray->pointer = (int *) calloc(1000, sizeof(int));
	newArray->size = 1000;
	newArray->counter = 0;
	return newArray;
}

void resizeArray(struct ArrayData* array) {
	int newSize = (array->size * sizeof(int)) * 2;
	array->pointer = (int *) realloc(array->pointer, newSize);
	fflush(stdout);
	array->size *= 2;  // This is the number of elements
}

int addElement(struct ArrayData* array, int number) {
	if (array->counter >= array->size) {
		resizeArray(array);
	}

	*(array->pointer + array->counter) = number;  // Pointer allocaton
	array->counter += 1;

	return 0;
}

int getElement(struct ArrayData* array, int index) {
	if (array->counter >= array->size) {
		return -1;
	}

	int* data = array->pointer + index;
	return *data;
}



int main() {
	struct ArrayData* array;
	array = initArray();

	int get;

	int i, data,j;
	int option;
	i = 0;

	//generating the initial 2000 elements and then asking for user input

	for (i = 0; i < 2000; i++) {
		addElement(array, rand() % 1000);
		get = getElement(array, i);
		//printf("%d\n", get);
	}

	
	while (1) {

		printf("\n\nEnter your choice:\n1. Insert element\n2. Display \n3. Exit\n >> ");
		scanf_s("%d", &option);

		if (option == 1) {
			i = i + 1;
			printf("Enter data to be inserted: ");
			scanf_s("%d", &data);
			addElement(array, data);
		}
		else if (option == 2) {
			//display the list
			for (j = 0; j < i; j++) {
				get = getElement(array, j);
				printf("%d\n", get);
			}
		}
		else if (option == 3) {
			break;
		}
	}
	
	free(array->pointer);
	free(array);
	return 0;
}