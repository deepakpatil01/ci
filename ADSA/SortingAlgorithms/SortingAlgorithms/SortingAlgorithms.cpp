// SortingAlgorithms.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "array.h"
#include <sys/stat.h>
#include <iostream>
#include <fstream>
#include <string>
#include <stdbool.h>

using namespace std;

statistics_t heap_sort(int* arr, int n);                // Heap Stort
statistics_t insertion_sort(int* arr, int n);           // Insertion
statistics_t merge_sort(int* arr, int n);               // Merge
statistics_t quick_sort(int* arr, int n);               // Quick

statistics_t(*sort_fun)(int* arr, int n);

//Heap Sort

void min_heapify(int* arr, int n, int i)
{
    // If right child is bigger, swap them and min_heapify subtrees
    if (2 * i + 2 < n)
    {
        if (*(arr + 2 * i + 2) > *(arr + i))
        {
            swap(arr + 2 * i + 2, arr + i);
            min_heapify(arr, n, 2 * i + 2);
        }
    }

    // If left child is bigger, swap them and min_heapify subtrees
    if (2 * i + 1 < n)
    {
        if (*(arr + 2 * i + 1) > *(arr + i))
        {
            swap(arr + 2 * i + 1, arr + i);
            min_heapify(arr, n, 2 * i + 1);
        }
    }


}


statistics_t heap_sort(int* arr, int n)
{
    printf("Sorting array with heap sort...\n\n");
    statistics_t ret = { 0 };
    uint64_t start_t = microsSinceEpoch();


    // Move the maximum element to the root
    for (int i = n / 2 - 1; i >= 0; i--)
    {
        min_heapify(arr, n, i);
    }

    // Move each element to the root and min_heapify again
    for (int i = n - 1; i > 0; i--)
    {
        swap(arr + i, arr);
        min_heapify(arr, i, 0);
    }

    ret.time = microsSinceEpoch() - start_t;
    return ret;
}

//binary tree

statistics_t binary_sort(int* arr, int n)
{
    printf("Sorting array with binary tree sort...\n\n");
    statistics_t ret = { 0 };

    uint64_t start_t = microsSinceEpoch();
    for (int i = 1; i < n; i++)
    {
        for (int j = i; j > 0; j--)
        {
            ret.comparisons++;
            ret.array_accesses += 2;
            if (*(arr + j) < *(arr + j - 1))
            {
                // Swap them!
                swap(arr + j, arr + j - 1);
                ret.array_accesses += 4;
            }
            else
            {
                break;
            }
        }
    }


    ret.time = microsSinceEpoch() - start_t;
    return ret;
}

//quick sort

int _partition(int* arr, int l, int r, statistics_t* ret)
{
    // Use first element (position l) as pivot
    int i = r;

    for (int j = r; j > l; j--)
    {
        ret->comparisons++;
        ret->array_accesses += 2;
        if (*(arr + j) > *(arr + l))
        {
            ret->array_accesses += 2;
            swap(arr + i, arr + j);
            i--;
        }
    }
    ret->array_accesses += 2;
    swap(arr + i, arr + l);
    return i;
}


void _quick_sort(int* arr, int l, int r, statistics_t* ret)
{
    if (l < r)
    {
        int p = _partition(arr, l, r, ret);
        _quick_sort(arr, l, p - 1, ret);
        _quick_sort(arr, p + 1, r, ret);
    }
}


statistics_t quick_sort(int* arr, int n)
{
    printf("Sorting array with quick sort...\n\n");
    statistics_t ret = { 0 };
    uint64_t start_time = microsSinceEpoch();

    _quick_sort(arr, 0, n - 1, &ret);

    ret.time = microsSinceEpoch() - start_time;
    return ret;
}

//merge sort

void _merge_sort(int* arr, int l, int r, statistics_t* ret);
void _merge(int* arr, int l, int m, int r, statistics_t* ret);

statistics_t merge_sort(int* arr, int n)
{
    printf("Sorting array with merge sort...\n\n");
    statistics_t ret = { 0 };
    uint64_t start_t = microsSinceEpoch();
    _merge_sort(arr, 0, n - 1, &ret);
    ret.time = microsSinceEpoch() - start_t;
    return ret;
}

void _merge_sort(int* arr, int l, int r, statistics_t* ret)
{
    if (r > l)
    {
        int m = (l + r) / 2;

        // Divide and conquer!
        _merge_sort(arr, l, m, ret);
        _merge_sort(arr, m + 1, r, ret);
        _merge(arr, l, m, r, ret);
    }

}

void _merge(int* arr, int l, int m, int r, statistics_t* ret)
{
    // Create temporary arrays
    int left_arr[m + 1 - l] = { 0 };
    int right_arr[r - m] = { 0 };

    // Fill them
    for (int i = 0; i < m + 1 - l; i++)
    {
        left_arr[i] = *(arr + l + i);
        ret->array_accesses++;
    }
    for (int i = 0; i < r - m; i++)
    {
        right_arr[i] = *(arr + m + 1 + i);
        ret->array_accesses++;
    }

    // Merge the two arrays sorted from low to high
    int i = 0;
    int j = 0;
    int k = l;
    while ((i < m + 1 - l) && (j < r - m))
    {
        ret->comparisons++;
        if (left_arr[i] < right_arr[j])
        {
            *(arr + k) = left_arr[i];
            i++;
            ret->array_accesses++;
        }
        else
        {
            *(arr + k) = right_arr[j];
            j++;
            ret->array_accesses++;
        }
        k++;
    }

    // There is usually one number hanging, place it into the merged array as well
    while (i < m + 1 - l)
    {
        *(arr + k) = left_arr[i];
        i++;
        k++;
        ret->array_accesses++;
    }
    while (j < r - m)
    {
        *(arr + k) = right_arr[j];
        j++;
        k++;
        ret->array_accesses++;
    }

}


int main(int argc, char* argv[])
{
    // Default values
    string mode = "low_to_high";
    string file = "test_data/100random.txt";
    bool verbose = false;
    bool print_stats = false;

    // User input overrides default values
    if (argc > 1)
    {
        for (int i = 1; i < argc; ++i)
        {
            string arg = argv[i];
            if ((arg == "-h") || (arg == "--help"))
            {
                //usage();
                return 0;
            }
            
            else if ((arg == "-f") || (arg == "--file"))
            {
                // Check that there are more parameters
                if (i + 1 < argc)
                {
                    file = "test_data/";
                    file += argv[++i];
                    struct stat buffer;
                    if (stat(file.c_str(), &buffer))
                    {
                        printf("%s is not a valid file\n", file.c_str());
                        return -1;
                    }
                }
            }
            else if ((arg == "-s") || (arg == "--stats"))
            {
                print_stats = true;
            }
            else if ((arg == "-a") || (arg == "--sort"))
            {
                // Check that there are more parameters
                if (i + 1 < argc)
                {
                    string sorting_alg = argv[++i];
                   if (sorting_alg == "heap")
                    {
                        sort_fun = &heap_sort;
                    }
                    else if (sorting_alg == "binary")
                    {
                        sort_fun = &binary_sort;
                    }
                    else if (sorting_alg == "merge")
                    {
                        sort_fun = &merge_sort;
                    }
                    else if (sorting_alg == "quick")
                    {
                        sort_fun = &quick_sort;
                    }
                  
                    else
                    {
                        printf("%s is not a valid sorting algorithm\n", sorting_alg.c_str());
                        return -1;
                    }

                }
            }
            else
            {
                printf("ERROR: Unkwnown parameter %s\n", arg.c_str());
                return -1;
            }
        }
    }
    if (sort_fun == nullptr)
    {
        printf("No sorting algorithm selected. Run -h for help\n");
        return -1;
    }



    // Load values to sort from the file
    string line = "";
    int n_rows = 0;
    ifstream random_file(file);
    if (random_file.is_open())
    {
        if (getline(random_file, line))
        {
            n_rows = stoi(line);
        }
        int random_array[n_rows] = { 0 };
        int i = 0;
        while (getline(random_file, line))
        {
            random_array[i] = stoi(line);
            i++;
        }
        random_file.close();
        if (verbose)
        {
            printf("Random array:\n");
            print_array(&random_array[0], n_rows);
            printf("\n");
        }

        // Sort the array!
        statistics_t stats = { 0 };
        stats = sort_fun(&random_array[0], n_rows);


        if (verbose)
        {
            printf("Sorted array:\n");
            print_array(&random_array[0], n_rows);
            printf("\n");
        }

        // Check that the array is indeed sorted
        bool sorted = true;
        for (int i = 1; i < n_rows; i++)
        {
            if (random_array[i - 1] > random_array[i])
            {
                sorted = false;
                if (verbose)
                {
                    printf("\t%d [%d] > %d [%d]\n", random_array[i - 1], i - 1, random_array[i], i);
                }
            }
        }
        if (sorted)
        {
            printf("\nThe array is correctly sorted!\n");
        }
        else
        {
            printf("\nThe array is NOT sorted!\n");
        }



        if (print_stats)
        {
            printf("\nStats for the sort:\n");
            printf("\tTotal time: %lu micros (%.3f seconds)\n", stats.time, stats.time / 1000000.0);
            printf("\tArray accesses: %lu\n", stats.array_accesses);
            printf("\tComparisons : %lu\n", stats.comparisons);
        }

    }
    else
    {
        printf("Could not open file '%s'\n", file.c_str());
        return -1;
    }
}