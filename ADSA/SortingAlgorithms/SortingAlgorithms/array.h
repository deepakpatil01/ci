//create the lib function to add / get elements from the array

#ifndef ARRAY_H_
#define ARRAY_H_

struct ArrayData* initArray();
int addElement(struct ArrayData* array, int number);
int getElement(struct ArrayData* array, int index);

struct ArrayData {
	int* pointer;
	int counter;
	int size;
};

#endif

#ifndef MAIN_INCLUDE_H
#define MAIN_INCLUDE_H
#include <cstdint>
#include <time.h>
#include <cstddef>
#include <cstdio>

struct statistics_t {
	uint64_t time;
	uint64_t comparisons;
	uint64_t array_accesses;
};

void print_array(int* arr, int n);
void swap(int* f, int* s);
uint64_t microsSinceEpoch(void);

#endif