#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
The graph that is built is given below (sample - the data is read from graph.txt)
               1
            /  |  \
           2   5   3 
          / \  | / |
         6     4   7 
*/
enum NodeStatus { Unvisited, Discovered, Complete };

/*
  Node for linked list of adjacent elements.   This contains a pointer for next node.
  It will not contain the real element but the index of element of the array containing all the vertices V.
*/
typedef struct list_node {
    int index_of_item;
    struct list_node* next;
}list_node;

/*
  Node to store the real element. This contains the data and pointer to the first element (head) of the adjacency list.
*/
typedef struct node {
    int data;
    enum NodeStatus nodeStatus;
    list_node* head;
}node;

/*
  Graph will contain number of vertices and an array containing all the nodes (V).
*/
typedef struct graph {
    int number_of_vertices;
    node heads[]; // array of nodes to store the list of first nodes of each adjacency list
}graph;

node* new_node(int data) {
    node* z;
    z = (node*)malloc(sizeof(node));
    z->data = data;
    z->head = NULL;
    z->nodeStatus = Unvisited;

    return z;
}

list_node* new_list_node(int item_index) {
    list_node* z;
    z = (list_node *)malloc(sizeof(list_node));
    z->index_of_item = item_index;
    z->next = NULL;

    return z;
}

// create a new graph
graph* new_graph(int number_of_vertices) {
    graph* gp = NULL;
    //number_of_vertices*sizeof(node) is the size of the array heads
    gp = (graph *)malloc(sizeof(graph) + (number_of_vertices * sizeof(node)));
    gp->number_of_vertices = number_of_vertices;

    //making elements of all head null i.e.,their data -1 and next null
    int i;
    for (i = 0; i < number_of_vertices; i++) {
        node* z = new_node(-1); //*z is pointer of node. z stores address of node
        gp->heads[i] = *z; //*z is the value at the address z
    }

    return gp;
}

// function to add new node to graph
void add_node_to_graph(graph* g, int data) {
    // creating a new node;
    node* z = new_node(data);
    //this node will be added into the heads array of the graph g
    int i;
    for (i = 0; i < g->number_of_vertices; i++) {
        // we will add node when the data in the node is -1
        if (g->heads[i].data < 0) {
            g->heads[i] = *z; //*z is the value at the address z
            break; //node is added
        }
    }
}

// function to check of the node is in the head array of graph or not
int in_graph_head_list(graph* g, int data) {
    int i;
    for (i = 0; i < g->number_of_vertices; i++) {
        if (g->heads[i].data == data)
            return 1;
    }
    return 0;
}

// function to add edge
void add_edge(graph* g, int source, int dest) {
    //if source or edge is not in the graph, add it
    if (!in_graph_head_list(g, source)) {
        add_node_to_graph(g, source);
    }
    if (!in_graph_head_list(g, dest)) {
        add_node_to_graph(g, dest);
    }

    int i, j;
    // iterating over heads array to find the source node
    for (i = 0; i < g->number_of_vertices; i++) {
        if (g->heads[i].data == source) { //source node found

            int dest_index; //index of destination element in array heads
            // iterating over heads array to find node containg destination element
            for (j = 0; j < g->number_of_vertices; j++) {
                if (g->heads[j].data == dest) { //destination found
                    dest_index = j;
                    break;
                }
            }

            list_node* n = new_list_node(dest_index); // new adjacency list node with destination index
            if (g->heads[i].head == NULL) { // no head, first element in adjaceny list
                g->heads[i].head = n;
            }
            else { // there is head which is pointer by the node in the head array
                list_node* temp;
                temp = g->heads[i].head;

                // iterating over adjaceny list to insert new list_node at last
                while (temp->next != NULL) {
                    temp = temp->next;
                }
                temp->next = n;
            }
            break;
        }
    }
}

void print_graph(graph* g) {
    int i;
    for (i = 0; i < g->number_of_vertices; i++) {
        list_node* temp;
        temp = g->heads[i].head;
        printf("%d\t", g->heads[i].data);
        while (temp != NULL) {
            printf("%d\t", g->heads[temp->index_of_item].data);
            temp = temp->next;
        }
        printf("\n");
    }
}

//handling the queue for BFS
typedef struct queue_node {
    node* n;
    struct queue_node* next;
}queue_node;

struct queue
{
    int count;
    queue_node* front;
    queue_node* rear;
};
typedef struct queue queue;

int is_empty_queue(queue* q)
{
    return !(q->count);
}

//Add a member to queue
void enqueue(queue* q, node* n)
{
    queue_node* new_queue_node;
    new_queue_node = (queue_node *) malloc(sizeof(queue_node));
    new_queue_node->n = n;
    new_queue_node->next = NULL;
    if (!is_empty_queue(q))
    {
        q->rear->next = new_queue_node;
        q->rear = new_queue_node;
    }
    else
    {
        q->front = q->rear = new_queue_node;
    }
    q->count++;
}
//remove a member from queue
queue_node* dequeue(queue* q)
{
    queue_node* tmp;
    tmp = q->front;
    q->front = q->front->next;
    q->count--;
    return tmp;
}

/*
 create a queue to be used for BFS
*/
queue* make_queue()
{
    queue* q;
    q = (queue *)malloc(sizeof(queue));
    q->count = 0;
    q->front = NULL;
    q->rear = NULL;
    return q;
}

/*
Display the queue
*/
void print_queue(queue* q) {
    queue_node* tmp;
    tmp = q->front;
    while (tmp != NULL) {
        printf("%d\t", tmp->n->data);
        tmp = tmp->next;
    }
    printf("\n");
}

void bfs(graph* g) {
    //here, we are using first node as source
    node s = g->heads[0];
    int i;
    for (i = 0; i < g->number_of_vertices; i++) {
        g->heads[i].nodeStatus = Unvisited;
    }
    s.nodeStatus = Discovered;

    queue* q = make_queue();
    enqueue(q, &s);

    while (!is_empty_queue(q)) {
        // print_queue(q);
        queue_node* u = dequeue(q);
        list_node* temp;
        temp = u->n->head;
        while (temp != NULL) {
            if (g->heads[temp->index_of_item].nodeStatus == Unvisited) {
                g->heads[temp->index_of_item].nodeStatus = Discovered;
                enqueue(q, &g->heads[temp->index_of_item]);
            }
            temp = temp->next;
        }
        u->n->nodeStatus = Complete;
        printf("%d ", u->n->data);
    }
}

void dfs_visit(graph* g, node* i) {
    i->nodeStatus = Discovered;

    list_node* temp;
    temp = i->head;
    while (temp != NULL) {
        if (g->heads[temp->index_of_item].nodeStatus == Unvisited) {
            dfs_visit(g, &g->heads[temp->index_of_item]);
        }
        temp = temp->next;
    }
    i->nodeStatus = Complete;
    printf("%d ", i->data);
}

void dfs(graph* g) {
    int i;
    for (i = 0; i < g->number_of_vertices; i++) {
        g->heads[i].nodeStatus = Unvisited;
    }

    for (i = 0; i < g->number_of_vertices; i++) {
        if (g->heads[i].nodeStatus == Unvisited) {
            dfs_visit(g, &g->heads[i]);
        }
    }
}


int main(int argc, char* argv[]) {
   
    FILE* stream; //file to hold graph data
    char delim[] = ",";
    const int maximumLineLength = 10; //number of character in line..this wont go beyond 4
    char vertexline[maximumLineLength]; //storage for the first line
    char nodeline[maximumLineLength]; //storage for the first line
    char* connectionArray[2];
    int index,counter=0;
    int vertices,edges = 0; // number of vertices in the graph
    int start = -999, end=-999;
    graph* graph = NULL; //the graph placeholder

    if (argc > 1) {
        stream = fopen(argv[1], "r");
        if (stream == NULL) {
            fprintf(stderr, "Cannot open file: %s\n", argv[1]);
            return 1;
        }
    }
    else {
        stream = stdin; /* read from standard input if no argument on the command line */
    }

    if (stream == NULL) //could be avoided - to be modified later
    {
        printf("The input stream is not readable.");
    }
    else
    {
        //read the file. The file structure is:
        /*  N # Number of vertices
            E # Number of edges
            vi, vj # edge ei that starts from vertex viand ends on vertex vj)
            vk, vl # edge ek that starts from vertex vkand ends on vertex vl)*/

        fgets(vertexline, sizeof(vertexline), stream);

        //identify the vertices based on string length
        //int length = strlen(vertexline);
        vertices = atoi(vertexline);

        if (vertices <= 0)
        {
            printf("Please enter Verices correctly in number format");
            return 0;
        }

        fgets(vertexline, sizeof(vertexline), stream);

        //identify the vertices based on string length
       
        edges = atoi(vertexline);

        if (edges <= 0)
        {
            printf("Please enter Edges correctly in number format");
            return 0;
        }

        //fix the arrary for the vertices
        graph = new_graph(vertices);
       
        //now the vertices are added..read the edge connection
        while (!feof(stream))
        {
            start = -999;
            end = -999;
            fgets(nodeline, sizeof(nodeline), stream);

            //split the connections
            char* connection = strtok(nodeline, delim);
            index = 0;
            while (connection != NULL)
            {
                if (index == 0)
                {
                    start = atoi(connection);
                }
                else
                {
                    end = atoi(connection);
                }

                connection = strtok(NULL, delim);
                index++;
            }
            if (start == -999 || end == -999) {
                printf("The edge connection is not correctly provided.");
                return 0;
            }
            else
            {
                add_edge(graph, start, end);
            }
           
            counter++;
        }
        if (edges != counter)
        {
            printf("The number of edges is not correctly provided.");
            return 0;
        }
        else
        {
            printf("DFS:\n");
            dfs(graph);
            printf("\nBFS:\n");
            bfs(graph);
        }

    }
   
    return 0;
}